<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    protected $primaryKey = 'people_id';
    protected $fillable = [
        'people_id', 'username', 'email', 'full_name', 'gender', 'followers_count', 'following_count',
        'following_tag_count', 'media_count', 'profile_pic_url', 'is_private', 'user_id',
        'friendship_status', 'is_business', 'reference_profile_user_id', 'is_active'
    ];

    public static function isNewPeople($people, $reference)
    {
        return People::where('people_id', $people->getPk())
            ->where('username', $people->getUsername())
            ->whereRaw('(reference_profile_user_id = "' . $reference . '" or reference_hashtag_user_id = "' . $reference . '" or reference_location_user_id = "' . $reference . '")')
            ->first();
    }



    public static function peopleToFollow($reference_array, $min, $max)
    {
        $rf_people = People::whereRaw('(reference_profile_user_id IN (' . "'" . implode("','", $reference_array['reference_profile']) . "'" . ') or reference_hashtag_user_id IN (' . "'" . implode("','", $reference_array['reference_hashtag']) . "'" . ') or reference_location_user_id IN (' . "'" . implode("','", $reference_array['reference_location']) . "'" . '))')
            ->where('friendship_status', false)
            ->where('is_active', false)
            ->take(mt_rand($min, $max))
            ->distinct()
            ->get();


        return $rf_people;
    }

    public static function deletePeopleById($id)
    {
        People::destroy($id);
    }

    public static function peopleToUnfollow($reference_array, $min, $max)
    {
        $rf_people = People::whereRaw('(reference_profile_user_id IN (' . "'" . implode("','", $reference_array['reference_profile']) . "'" . ') or reference_hashtag_user_id IN (' . "'" . implode("','", $reference_array['reference_hashtag']) . "'" . ') or reference_location_user_id IN (' . "'" . implode("','", $reference_array['reference_location']) . "'" . '))')
            ->whereRaw("ADDTIME(updated_at,'8:0') <= CURRENT_TIME()")
            ->where('friendship_status', true)
            ->where('is_active', true)
            ->take(mt_rand($min, $max))
            ->distinct()
            ->inRandomOrder()
            ->get();


        return $rf_people;
    }

    public static function getAllReferenceFollowers($reference_array, $friendship_status, $is_active)
    {
        return People::whereRaw('(reference_profile_user_id IN (' . "'" . implode("','", $reference_array['reference_profile']) . "'" . ') or reference_hashtag_user_id IN (' . "'" . implode("','", $reference_array['reference_hashtag']) . "'" . ') or reference_location_user_id IN (' . "'" . implode("','", $reference_array['reference_location']) . "'" . '))')
            ->where('friendship_status', $friendship_status)
            ->where('is_active', $is_active)
            ->orderBy("created_at", "asc")
            ->get();
    }

    public static function getAllReferenceFollowersByDate($reference, $friendship_status, $is_active, $date)
    {
        return People::whereRaw('(reference_profile_user_id = "' . $reference . '" or reference_hashtag_user_id = "' . $reference . '" or reference_location_user_id = "' . $reference . '")')
            ->whereDate('updated_at', '>=', $date)
            ->where('friendship_status', $friendship_status)
            ->where('is_active', $is_active)
            ->distinct()
            ->get();
    }

    /**
     * Find People Followed in the last hourd
     */
    public static function getFollowedHourly($reference_array)
    {
        return People::whereRaw('(reference_profile_user_id IN (' . "'" . implode("','", $reference_array['reference_profile']) . "'" . ') or reference_hashtag_user_id IN (' . "'" . implode("','", $reference_array['reference_hashtag']) . "'" . ') or reference_location_user_id IN (' . "'" . implode("','", $reference_array['reference_location']) . "'" . '))')
            ->where('updated_at', '>=', now()->subHour(2)->toDateTimeString())
            ->where('friendship_status', true)
            ->where('is_active', true)->count();
    }



    public static function deletePeople($reference_profile_user_id)
    {
        People::where('reference_profile_user_id', $reference_profile_user_id)
            ->where('friendship_status', true)->delete();
    }

    public static function findFollower($people_id, $reference_array)
    {
        return People::whereRaw('(reference_profile_user_id IN (' . "'" . implode("','", $reference_array['reference_profile']) . "'" . ') or reference_hashtag_user_id IN (' . "'" . implode("','", $reference_array['reference_hashtag']) . "'" . ') or reference_location_user_id IN (' . "'" . implode("','", $reference_array['reference_location']) . "'" . '))')
            ->where('people_id', $people_id)
            ->orderBy("created_at", "asc")
            ->first();
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPk()
    {
        return $this->people_id;
    }

    /**
     * @return mixed
     */
    public function getPeopleId()
    {
        return $this->people_id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->full_name;
    }

    /**
     * @param mixed $full_name
     */
    public function setFullName($full_name): void
    {
        $this->full_name = $full_name;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getFollowerCount()
    {
        return $this->followers_count;
    }

    /**
     * @param mixed $followers_count
     */
    public function setFollowersCount($followers_count): void
    {
        $this->followers_count = $followers_count;
    }

    /**
     * @return mixed
     */
    public function getFollowingCount()
    {
        return $this->following_count;
    }

    /**
     * @param mixed $following_count
     */
    public function setFollowingCount($following_count): void
    {
        $this->following_count = $following_count;
    }

    /**
     * @return mixed
     */
    public function getFollowingTagCount()
    {
        return $this->following_tag_count;
    }

    /**
     * @param mixed $following_tag_count
     */
    public function setFollowingTagCount($following_tag_count): void
    {
        $this->following_tag_count = $following_tag_count;
    }

    /**
     * @return mixed
     */
    public function getMediaCount()
    {
        return $this->media_count;
    }

    /**
     * @param mixed $media_count
     */
    public function setMediaCount($media_count): void
    {
        $this->media_count = $media_count;
    }

    /**
     * @return mixed
     */
    public function getProfilePicUrl()
    {
        return $this->profile_pic_url;
    }

    /**
     * @param mixed $profile_pic_url
     */
    public function setProfilePicUrl($profile_pic_url): void
    {
        $this->profile_pic_url = $profile_pic_url;
    }

    /**
     * @return mixed
     */
    public function getisPrivate()
    {
        return $this->is_private;
    }

    /**
     * @param mixed $is_private
     */
    public function setIsPrivate($is_private): void
    {
        $this->is_private = $is_private;
    }
}
