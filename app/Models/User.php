<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

//User Model
class User extends Authenticatable implements JWTSubject
{

    // Rest omitted for brevity
    protected $primaryKey = 'user_id'; // or null
    protected $table = 'users';
    protected $fillable = ['username', 'password', 'password_string', 'user_id'];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }




}
