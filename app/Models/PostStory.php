<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostStory extends Model
{
    protected $table = "post_story";
    protected $guarded = [];
    protected $hidden = ['created_at', 'updated_at'];
    protected $with = ['postMedia','storyHashtags','storyUserTags'];



    public function postMedia()
    {
        return $this->belongsToMany(Media::class, 'post_media', 'post_story_id', 'media_id');
    }

    public static function getPosts()
    {
        return PostStory::where('already_posted', false)->get();
    }

    public function storyUserTags()
    {
        return $this->hasMany(TaggedPeople::class, 'tagged_people_id', 'tagged_people_id');
    }

    public function storyHashtags()
    {
        return $this->hasMany(TaggedHashtag::class, 'tagged_hashtag_id', 'tagged_hashtag_id');
    }
}
