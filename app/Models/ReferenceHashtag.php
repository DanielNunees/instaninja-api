<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReferenceHashtag extends Model
{
    protected $fillable = ['user_id', 'hashtag', 'total_likes','reference_hashtag_user_id'];

    public static function newUserHashtag($user_id, $hashtag, $user_hashtag_reference_id)
    {
        return ReferenceHashtag::firstOrCreate(['user_id' => $user_id, 'hashtag' => $hashtag, 'reference_hashtag_user_id' => $user_hashtag_reference_id]);
    }

    public static function getRandomHashtag($user_id, $quantity)
    {
        return ReferenceHashtag::where('user_id', $user_id)
            ->take($quantity)
            ->where('is_active', true)
            ->inRandomOrder()
            ->get();
    }

    public static function updateFollowBack($reference_hashtag_user_id, $follow_back_count)
    {
        $rp_user = ReferenceHashtag::where('reference_hashtag_user_id',$reference_hashtag_user_id)->where('is_active', true)->first();
        $rp_user->follow_back += $follow_back_count;
        $rp_user->save();
    }

    public static function incrementFollowers($reference_hashtag_user_id)
    {
        $rp_user = ReferenceHashtag::getReferenceHashtag($reference_hashtag_user_id);
        $rp_user->total_follow++;
        $rp_user->save();
    }

    public static function getAllReferenceHashtag($user_id)
    {
        return ReferenceHashtag::where('user_id', $user_id)->get();
    }

    public static function getReferenceHashtagUserIdList($user_id)
    {
        $rf = ReferenceHashtag::where('user_id', $user_id)->select('reference_hashtag_user_id')->where('is_active', true)->get();
        $reference_hashtag_user_id_array = array();
        foreach ($rf as $item) {
            $reference_hashtag_user_id_array[] = $item->reference_hashtag_user_id;
        }
        return $reference_hashtag_user_id_array;
    }
}
