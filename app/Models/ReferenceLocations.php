<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReferenceLocations extends Model
{
    protected $guarded = ['latitude', 'longitude', 'address'];

    public static function newUserReferenceLocation($user_id, $location, $reference_location_user_id)
    {
        return ReferenceLocations::updateOrCreate([
            'user_id' => $user_id,
            'external_id' => $location->getExternalId(),
            'latitude' => $location->getLat(),
            'longitude' => $location->getLng(),
            'address' => $location->getAddress(),
            'name' => $location->getName(),
            'reference_location_user_id' => $reference_location_user_id,
            'is_active' => true
        ]);
    }

    public static function getRandomLocation($user_id, $qtd = 1)
    {
        return ReferenceLocations::where('user_id', $user_id)
            ->take($qtd)
            ->where('is_active', true)
            ->inRandomOrder()
            ->get();
    }

    public static function updateFollowBack($reference_location_user_id, $follow_back_count)
    {
        $rp_user = ReferenceLocations::where('reference_location_user_id', $reference_location_user_id)->where('is_active', true)->first();
        $rp_user->follow_back += $follow_back_count;
        $rp_user->save();
    }

    public static function getAllReferenceLocation($user_id)
    {
        return ReferenceLocations::where('user_id', $user_id)->get();
    }


    public static function getReferenceLocationUserIdList($user_id)
    {
        $rf = ReferenceLocations::where('user_id', $user_id)->select('reference_location_user_id')->where('is_active', true)->get();
        $reference_location_user_id_array = array();
        foreach ($rf as $item) {
            $reference_location_user_id_array[] = $item->reference_location_user_id;
        }
        return $reference_location_user_id_array;
    }
}
