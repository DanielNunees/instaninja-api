<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReferenceProfile extends Model
{
    //protected $primaryKey = 'people_id';
    protected $fillable = [
        'user_id', 'people_id', 'total_likes', 'username', 'total_follow', 'follow_back',
        'followers_max_id', 'reference_profile_user_id', 'is_active'
    ];


    public static function updateFollowBack($reference_profile_user_id, $follow_back_count)
    {
        $rp_user = ReferenceProfile::where('reference_profile_user_id', $reference_profile_user_id)->where('is_active', true)->first();
        $rp_user->follow_back += $follow_back_count;
        $rp_user->save();
    }

    public static function getReferenceProfile($reference_profile_user_id)
    {
        return ReferenceProfile::where('reference_profile_user_id', $reference_profile_user_id)->where('is_active', true)->first();
    }

    public static function getAllReferenceProfiles($user_id)
    {
        return ReferenceProfile::where('user_id', $user_id)->get();
    }

    /**
     * @param $user_id
     * @param int $count
     * @return mixed
     */
    public static function getRandomProfile($user_id)
    {
        return ReferenceProfile::where('user_id', $user_id)
            ->where('is_active', true)
            ->inRandomOrder()
            ->first();
    }

    public static function incrementFollowers($reference_profile_user_id)
    {
        $rp_user = ReferenceProfile::getReferenceProfile($reference_profile_user_id);
        $rp_user->total_follow++;
        $rp_user->save();
    }

    public static function getReferenceProfileUserIdList($user_id)
    {
        $rf = ReferenceProfile::where('user_id', $user_id)->select('reference_profile_user_id')->where('is_active', true)->get();
        $reference_profile_user_id_array = array();
        foreach ($rf as $item) {
            $reference_profile_user_id_array[] = $item->reference_profile_user_id;
        }
        return $reference_profile_user_id_array;
    }

    public static function getAllReferenceProfileUserIdList($user_id)
    {
        $rf = ReferenceProfile::where('user_id', $user_id)->select('reference_profile_user_id')->get();
        $reference_profile_user_id_array = array();
        foreach ($rf as $item) {
            $reference_profile_user_id_array[] = $item->reference_profile_user_id;
        }
        return $reference_profile_user_id_array;
    }

    public static function checkReferenceProfile($user, $username)
    {
        return ReferenceProfile::where('username', $username)
            ->where('user_id', $user->user_id)->where('is_active', true)->get();
    }

    public static function getFollowedBackToday($reference_profile_user_id_array)
    {
        return ReferenceProfile::where('user_id', $reference_profile_user_id_array)
            ->where('updated_at', '>=', now()->toDateString())
            ->where('is_active', true)
            ->sum('follow_back');
    }
}
