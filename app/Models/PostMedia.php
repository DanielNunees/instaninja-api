<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostMedia extends Model
{
    protected $fillable = ['media_id', 'post_order', 'post_timeline_id', 'post_story_id'];


    public function medias(){
        return $this->hasMany('App\Media','id','media_id')->get();
    }
}
