<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserConfiguration extends Model
{
    protected $table = 'user_configuration';
    protected $fillable = [
        'user_id', 'stop_follow_request', 'throttled_request_time', 'max_following', 'follow_max',
        'follow_min', 'follow_time_max', 'follow_time_min', 'unfollow_max', 'unfollow_min',
        'unfollow_time_max', 'unfollow_time_min', 'like_max', 'like_min', 'like_time_max',
        'like_time_min', 'daily_posts', 'daily_stories', 'schedule_timeline_post', 'schedule_story_post'];

    public static function newUserConfiguration($user_id, $user_configuration)
    {

        if (!isset($user_configuration['stop_follow_request'])) {
            $user_config = new UserConfiguration();
            $user_config->user_id = $user_id;
            $user_config->save();
            return true;
        }
        UserConfiguration::updateOrCreate(['user_id' => $user_id], [
            'stop_follow_request' => $user_configuration['stop_follow_request'], 'throttled_request_time' => $user_configuration['throttled_request_time'],
            'max_following' => $user_configuration['max_following'], 'follow_max' => $user_configuration['follow_max'],
            'follow_min' => $user_configuration['follow_min'], 'follow_time_max' => $user_configuration['follow_time_max'],
            'follow_time_min' => $user_configuration['follow_time_min'], 'unfollow_max' => $user_configuration['unfollow_max'],
            'unfollow_min' => $user_configuration['unfollow_min'], 'unfollow_time_max' => $user_configuration['unfollow_time_max'],
            'unfollow_time_min' => $user_configuration['unfollow_time_min'], 'like_max' => $user_configuration['like_max'],
            'like_min' => $user_configuration['like_min'], 'like_time_max' => $user_configuration['like_time_max'],
            'like_time_min' => $user_configuration['like_time_min'], 'schedule_story_post' => $user_configuration['schedule_story_post'],
            'daily_posts' => $user_configuration['daily_posts'], 'daily_stories' => $user_configuration['daily_stories'],
            'schedule_timeline_post' => $user_configuration['schedule_timeline_post']]);
    }
}
