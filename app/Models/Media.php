<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $fillable = ['instagram_media_id','file_name', 'folder_name', 'media_type', 'url'];

    public static function getRandonMediaFromFolder($folder_name, $quantity = 1)
    {
        return Media::take($quantity)->whereIn("folder_name", $folder_name)->inRandomOrder()->get();
    }
}
