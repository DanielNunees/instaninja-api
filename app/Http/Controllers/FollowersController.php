<?php

namespace App\Http\Controllers;


use App\Models\ActionSchedule;
use App\Models\UserConfiguration;

class FollowersController extends Controller
{

    private $user_configuration;


    /**
     * Main function to follow/unfollow people
     * get the references profiles, these followers
     * and transform this array in to slices to
     * the startFollow function
     *
     *
     * @throws \Exception
     */
    public function startFollowFollowers()
    {
        try {
            $user = auth()->userOrFail();
            $this->user_configuration = UserConfiguration::where('user_id', $user->user_id)->first();
            $reference_list = UserController::getAllReferencesIdList($user->user_id);
            if (count($reference_list['reference_profile']) && count($reference_list['reference_hashtag']) && count($reference_list['reference_location'])) {
                return response()->json(["info" => "You must register some references first."], 200);
            }
            self::updateOrCreateTimes($user->user_id);
            return response()->json(["success" => "Requested"], 200);
        } catch (\Exception $ex) {
            LogController::error($user, $ex->getMessage(), 'Follow people request', $ex->getCode());
            return response()->json(["error" => "Follow request not respondig"], 201);
        }
    }

    /**
     * Update or create the follow and unfollow time when the user request at
     * the dashboard
     */

    public function updateOrCreateTimes($user_id)
    {
        ActionSchedule::updateOrCreate(
            [
                "user_id" => $user_id
            ],
            [
                "follow_time" => self::followTime(),
                "unfollow_time" => self::unfollowTime(),
                "active" => true
            ]
        );
    }

    /**
     * Calculate the time to start a new follow job including the throttled_request_time if > 0
     */
    public function followTime()
    {
        $follow_time = now()->addMinutes(mt_rand($this->user_configuration->follow_time_min, $this->user_configuration->follow_time_max));
        $follow_time = now()->createFromTimeString($follow_time)->addMinutes($this->user_configuration->throttled_request_time);
        return $follow_time;
    }

    /**
     * Calculate the time to start a new unfollow job including the throttled_request_time if > 0
     */
    public function unfollowTime()
    {
        $unfollow_time = now()->addMinutes(mt_rand($this->user_configuration->unfollow_time_min, $this->user_configuration->unfollow_time_max));
        $unfollow_time = now()->createFromTimeString($unfollow_time)->addMinutes($this->user_configuration->throttled_request_time);
        return $unfollow_time;
    }

    public function stopFollowFollowers()
    {
        try {
            $user = auth()->userOrFail();
            $action_scheduled = ActionSchedule::where('user_id', $user->user_id)->first();
            $action_scheduled->follow_time = null;
            $action_scheduled->unfollow_time = null;
            $action_scheduled->save();
            if ($action_scheduled) {
                return response()->json(["success" => "Request Stoped"], 200);
            }
            return  response()->json(["info" => "Request is not running"], 200);
        } catch (\Exception $ex) {
            LogController::error(auth()->user(), $ex->getMessage(), 'Stop Following Request', $ex->getCode());
            return response()->json(["error" => "Something went wrong"], 200);
        }
    }
}
