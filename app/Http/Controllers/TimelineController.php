<?php

namespace App\Http\Controllers;

use App\Models\PostMedia;
use App\Models\PostTimeline;
use Illuminate\Http\Request;
use Validator;

class TimelineController extends Controller
{

    /**
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    public function createNewPost(Request $request)
    {
        try {
             $new_post = $this->validator($request); 
            $tagged_people_id = PeopleController::taggedPeople($request->tagged_people);
            $media =  MediaController::store($request);
            if (!is_object($media)) return $media;
            $user = auth()->userOrFail();
            if ($new_post['post_location']) {
                $location =  json_decode($new_post['post_location'], true);
            }
            $timeline_post = PostTimeline::updateOrCreate(
                [
                    'id' => $new_post["id"],
                    'user_id' => $user->user_id
                ],
                [
                    'location_lat' => isset($location['lat']) ? $location['lat'] : null,
                    'location_lng' => isset($location['lng']) ? $location['lng'] : null,
                    'comment' => isset($new_post['comment']) ? $new_post['comment'] : null,
                    'recurrent_post_frequency' => isset($new_post['recurrent_post_frequency']) ? $new_post['recurrent_post_frequency'] : null,
                    'tagged_people_id' => $tagged_people_id,
                    'caption' => $new_post['caption'], //required
                    'post_at' => $new_post['post_at'], //required
                ]
            );
            if ($media) {
                PostMedia::updateOrCreate(
                    [
                        'post_timeline_id' => $timeline_post->id
                    ],
                    [
                        'media_id' => $media->id,
                        'post_order' => isset($media->post_order) ? $media->post_order : 1,
                    ]
                );
            }
            return response()->json(["success" => "Post created and scheduled"], 200);
        } catch (\Exception $ex) {
            LogController::error(auth()->user(), $ex->getMessage(), 'Create new post', $ex->getCode());
            return response()->json(["error" => "Something went wrong"], 200);
        }
    }

    /**
     * @param Request $request
     * @return array|null|string
     * @throws \Exception
     */
    public function validator(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'caption' => 'required|string',
            'post_at' => 'required|date',
            'photo' => 'required'
        ]);
        if ($validator->fails()) {
            throw new \Exception($validator->errors());
        } else {
            return ($request->all());
        }
    }

    /**
     * Get all post from the logged user (posteds and scheduleds)
     */
    public function getAll()
    {
        try {
            $user = auth()->userOrFail();
            return PostTimeline::get()->where('user_id', $user->user_id)->groupBy('already_posted');
        } catch (\Exception $ex) {
            LogController::error(auth()->user(), $ex->getMessage(), 'Get all posts', $ex->getCode());
            return response()->json(["error" => "Something went wrong"], 200);
        }
    }

    /**
     * Get a specific post
     */
    public function getPost($id)
    {
        try {
            $user = auth()->userOrFail();
            return PostTimeline::where('user_id', $user->user_id)->where('id', $id)->get();
        } catch (\Exception $ex) {
            LogController::error(auth()->user(), $ex->getMessage(), 'Get post', $ex->getCode());
            return response()->json(["error" => "Something went wrong"], 200);
        }
    }
}
