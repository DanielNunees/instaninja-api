<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\PostStory;
use App\Models\PostMedia;

class StoryController extends Controller
{
    /**
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    public function createNewStory(Request $request)
    {
        try {
            return $response = ApiController::postApiMultiPart('story/new', $request->all());
            return response()->json(["success" => "Story created and scheduled"], 200);
        } catch (\Exception $ex) {
            LogController::error(auth()->user(), $ex->getMessage(), 'Create new Story', $ex->getCode());
            return response()->json(["error" => "Something went wrong"], 200);
        }
    }

    /**
     * @param Request $request
     * @return array|null|string
     * @throws \Exception
     */
    public function validator(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'post_at' => 'required|date',
            'photo' => 'required'
        ]);
        if ($validator->fails()) {
            throw new \Exception($validator->errors());
        } else {
            return ($request->all());
        }
    }

    /**
     * Get all post from the logged user (posteds and scheduleds)
     */
    public function getAll()
    {
        try {
            $user = auth()->userOrFail();
            return PostStory::get()->where('user_id', $user->user_id)->groupBy('already_posted');
        } catch (\Exception $ex) {
            LogController::error(auth()->user(), $ex->getMessage(), 'Get all posts', $ex->getCode());
            return response()->json(["error" => "Something went wrong"], 200);
        }
    }

    /**
     * Get a specific post
     */
    public function getPost($id)
    {
        try {
            $user = auth()->userOrFail();
            return PostStory::where('user_id', $user->user_id)->where('id', $id)->get();
        } catch (\Exception $ex) {
            LogController::error(auth()->user(), $ex->getMessage(), 'Get post', $ex->getCode());
            return response()->json(["error" => "Something went wrong"], 200);
        }
    }
}
