<?php

namespace App\Http\Controllers;

use App\Models\TaggedPeople;
use App\Models\Comments;
use App\Models\TaggedHashtag;

class PostController extends Controller
{

    public function postOptions()
    {
        try {
            $user = auth()->userOrFail();
            $comments = Comments::where('user_id', $user->user_id)->get();
            $tagged_people = TaggedPeople::where('user_id', $user->user_id)->get();
            $tagged_hashtag = TaggedHashtag::where('user_id', $user->user_id)->get();
            return response()->json([
                "comments" => $comments,
                'tagged_people' => $tagged_people,
                'tagged_hashtag' => $tagged_hashtag
            ], 200);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }
}
