<?php

namespace App\Http\Controllers;

use App\Models\ActionSchedule;
use App\Models\UserConfiguration;

class LikesController extends Controller
{

    private $user_configuration;

    /**
     * LikesController constructor.
     */
    public function __construct()
    { }

    /**
     * Like Hashtag, my own feed and location medias
     * @throws \Exception
     */
    public function startLikes()
    {
        try {
            $user = auth()->userOrFail();
            $this->user_configuration = UserConfiguration::where('user_id', $user->user_id)->first();
            self::createTimes($user->user_id);
            return response()->json(["success" => "Requested"], 200);
        } catch (\Exception $ex) {
            LogController::error($user, $ex->getMessage(), 'Follow people request', $ex->getCode());
            return response()->json(["error" => "Follow request not respondig"], 201);
        }
    }

    public function createTimes($user_id)
    {
        ActionSchedule::updateOrCreate(
            [
                "user_id" => $user_id
            ],
            [
                "like_time" => self::likeTime(),
                "active" => true
            ]
        );
    }

    /**
     * Calculate the time to start a new like job including the throttled_request_time if > 0
     */
    public function likeTime()
    {
        $like_time = now()->addMinutes(mt_rand($this->user_configuration->like_time_min, $this->user_configuration->like_time_max));
        $like_time = now()->createFromTimeString($like_time)->addMinutes($this->user_configuration->throttled_request_time);
        return $like_time;
    }

    public function stopLikes()
    {
        try {
            $user = auth()->userOrFail();
            $action_scheduled = ActionSchedule::where('user_id', $user->user_id)->first();
            $action_scheduled->like_time = null;
            $action_scheduled->save();
            if ($action_scheduled) {
                return response()->json(["success" => "Request Stoped"], 200);
            }
            return  response()->json(["info" => "Request is not running"], 200);
        } catch (\Exception $ex) {
            LogController::error(auth()->user(), $ex->getMessage(), 'Stop Following Request', $ex->getCode());
            return response()->json(["error" => "Something went wrong"], 200);
        }
    }
}
