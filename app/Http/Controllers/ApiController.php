<?php

namespace App\Http\Controllers;

use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

use JWTAuth;
use function GuzzleHttp\json_decode;

class ApiController extends Controller
{
    public static function postApi($url, $value)
    {
        try {
            $token = AuthController::apiLogin();
            $token_decoded =  json_decode($token);
            $endpoint = "http://localhost/instaninjaservice/public/api/" . $url;

            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', $endpoint, [
                'http_errors' => false,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token_decoded->token
                ],
                'form_params' => $value
            ]);
            return $response->getBody()->getContents();
        } catch (ClientException $ex) {
            LogController::error(auth()->user(), Psr7\str($ex->getResponse()), 'Post Api', 400);
            return response()->json(["message" => [($ex->getMessage())]], 400);
        } catch (ServerException $ex) {
            LogController::error(auth()->user(), Psr7\str($ex->getResponse()), 'Post Api', 400);
            return response()->json(["message" => [($ex->getMessage())]], 400);
        } catch (\Exception $ex) {
            LogController::error(auth()->user(), ($ex->getResponse()), 'Post Api', 400);
            return response()->json(["message" => [($ex->getMessage())]], 400);
        }
    }

    public static function postApiRegister($url, $value)
    {
        try {
            $endpoint = "http://localhost/instaninjaservice/public/api/" . $url;

            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', $endpoint, [
                'http_errors' => false,
                'form_params' => $value
            ]);
            return response()->json(["message" => [$response->getBody()->getContents()]], $response->getStatusCode());
        } catch (ClientException $ex) {
            LogController::error(null, Psr7\str($ex->getResponse()), 'Get Register Api', 401);
            return response()->json(["message" => [Psr7\str($ex->getResponse())]], 400);
        } catch (ServerException $ex) {
            LogController::error(null, Psr7\str($ex->getResponse()), 'Get Register Api', 402);
            return response()->json(["message" => [(Psr7\str($ex->getResponse()))]], 400);
        } catch (\Exception $ex) {
            LogController::error(null, ($ex->getMessage()), 'Get Register Api', 403);
            return response()->json(["message" => [($ex->getMessage())]], 400);
        }
    }

    public static function postApiMultiPart($url, $values)
    {
        $multipart = [];
        foreach ($values as $key => $value) {
            $multipart[] = [
                'name'     => $key,
                'contents' => $value
            ];
        }

        try {
            $token = AuthController::apiLogin();

            $token_decoded =  json_decode($token);
            $endpoint = "http://localhost/instaninjaservice/public/api/" . $url;

            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', $endpoint, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token_decoded->token
                ],
                "multipart" => $multipart
            ]);
            return (json_decode(($response->getBody()->getContents())));
        } catch (\Exception $ex) {
            LogController::error(auth()->user(), $ex->getMessage(), 'Store Media', $ex->getCode());
            throw new \Exception($ex->getMessage());
        }
    }


    public static function getApi($url)
    {
        try {
            $token = AuthController::apiLogin();
            $token_decoded =  json_decode($token);
            $endpoint = "http://localhost/instaninjaservice/public/api/" . $url;

            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', $endpoint,  [
                'http_errors' => false,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token_decoded->token,
                ]
            ]);
            return ($response->getBody()->getContents());
        } catch (ClientException $ex) {
            LogController::error(auth()->user(), Psr7\str($ex->getResponse()), 'Get Api', 401);
            return response()->json(["message" => [($ex->getMessage())]], 400);
        } catch (ServerException $ex) {
            LogController::error(auth()->user(), Psr7\str($ex->getResponse()), 'Get Api', 402);
            return response()->json(["message" => [($ex->getMessage())]], 400);
        } catch (\Exception $ex) {
            LogController::error(auth()->user(), $ex, 'Get Api', 403);
            return response()->json(["message" => [($ex->getMessage())]], 400);
        }
    }
}
