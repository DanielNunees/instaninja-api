<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;


class MediaController extends Controller
{
    public static function store(Request $request)
    {

        try {
            $user = JWTAuth::parseToken()->authenticate();
            $token = AuthController::apiLogin();

            $token_decoded =  json_decode($token);
            $endpoint = "http://localhost/instaninjaservice/public/api/media/new";

            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', $endpoint, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token_decoded->token
                ],
                'form_params' => $request->all(),
                'multipart' => [
                    [
                        'name'     => 'photo',
                        'contents' => fopen($request->photo, 'r')
                    ],

                ]
            ]);
            return (json_decode(($response->getBody()->getContents())));
        } catch (\Exception $ex) {
            LogController::error(auth()->user(), $ex->getMessage(), 'Store Media', $ex->getCode());
            return response()->json(["error" => "Something went wrong"], 200);
        }
    }
}
