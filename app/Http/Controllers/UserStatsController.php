<?php

namespace App\Http\Controllers;

use App\Models\LikesStats;
use App\Models\People;
use App\Models\ExceptionsLog;
use App\Models\User;
use App\Models\UserStats;
use App\Models\UserLog;
use JWTAuth;
use App\Models\SystemLog;
use App\Models\ActionSchedule;

class UserStatsController extends Controller
{

    private $users;
    private $logged_user;

    /**
     * FollowersController constructor.
     */
    public function __construct()
    {
        $this->users = User::all();
    }


    public function getUserStats($quantity = null)
    {
        $logged_user = auth()->userOrFail();
        if (!is_null($logged_user)) {
            $response =  UserStats::where('user_id', $logged_user->user_id)
                ->orderBy('created_at', 'desc')
                ->first();
            return response()->json($response, 200);
        }
    }

    /**
     * Check if the Follow Followers and Like All is running (so far)
     * 
     */
    public function checkStatus()
    {
        $user = auth()->userOrFail();
        $actions = ActionSchedule::where('user_id', $user->user_id)->first();

        if (is_null($actions) || !isset($actions)) {
            $status["follow"] = false;
            $status["likes"] = false;
        } else {
            if (!$actions->active) {
                $status["follow"] = false;
                $status["likes"] = false;
            } else {
                $status["follow"] = $actions->follow_time > 0 ? true : false;
                $status["likes"] = $actions->like_time > 0 ? true : false;
            }
        }

        return response()->json($status, 200);
    }


    /**
     * Get User actions logs
     */
    public function getUserLogs($quantity)
    {
        $logged_user = auth()->userOrFail();
        $response = UserLog::where('user_id', $logged_user->user_id)
            ->take($quantity)
            ->orderBy('created_at', 'desc')
            ->get();
        return response()->json($response, 200);
    }

    /**
     * Get System Exception Logs
     */
    public function getExceptionLogs($quantity)
    {
        $logged_user = auth()->userOrFail();
        $response = ExceptionsLog::where('user_id', $logged_user->user_id)
            ->take($quantity)
            ->orderBy('created_at', 'desc')
            ->get();
        return response()->json($response, 200);
    }

    /**
     * Get system logs
     */
    public function getSystemLogs($quantity)
    {
        $logged_user = auth()->userOrFail();
        $response = SystemLog::where('user_id', $logged_user->user_id)
            ->take($quantity)
            ->orderBy('created_at', 'desc')
            ->get();
        return response()->json($response, 200);
    }
}
