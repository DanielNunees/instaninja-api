<?php

namespace App\Http\Controllers;

use App\Models\ReferenceHashtag;
use App\Models\ReferenceLocations;
use App\Models\ReferenceProfile;
use App\Models\User;
use App\Models\UserConfiguration;
use Illuminate\Http\Request;
use JWTAuth;
use Validator;
use PHPUnit\Framework\Exception;

class UserController extends Controller
{

    /**
     * @return mixed
     */
    public static function getUserConfiguration($user_id)
    {
        return UserConfiguration::where('user_id', $user_id)->first();
    }

    public function registerUserSettings(Request $request)
    {
        try {
            $user = auth()->userOrFail();
            UserConfiguration::updateOrCreate(['user_id' => $user->user_id], $request->all());
            return response()->json(["success" => "Settings updated"], 200);
        } catch (\Exception $ex) {
            LogController::error($user, $ex->getMessage(), "Register User Settings", $ex->getCode());
            return response()->json(["error" => "Something is wrong"], 200);
        }
    }

    public function getUserSettings()
    {
        $user = auth()->userOrFail();
        if (!is_null($user)) {
            $response = UserConfiguration::where('user_id', $user->user_id)->get();
            return response()->json($response, 200);
        }
        return response()->json(["User not found or not authenticated"], 401);
    }

    public function getAllReferences()
    {
        $user = auth()->userOrFail();
        $reference_people = ReferenceProfile::getAllReferenceProfiles($user->user_id);
        $reference_hashtag = ReferenceHashtag::getAllReferenceHashtag($user->user_id);
        $reference_location = ReferenceLocations::getAllReferenceLocation($user->user_id);

        return array("reference_profile" => $reference_people, "reference_hashtag" => $reference_hashtag, "reference_location" => $reference_location);
    }

    public static function getAllReferencesIdList($user_id)
    {
        $reference_people = ReferenceProfile::getReferenceProfileUserIdList($user_id);
        $reference_hashtag = ReferenceHashtag::getReferenceHashtagUserIdList($user_id);
        $reference_location = ReferenceLocations::getReferenceLocationUserIdList($user_id);

        return array("reference_profile" => $reference_people, "reference_hashtag" => $reference_hashtag, "reference_location" => $reference_location);
    }

    public static function register($user)
    {
        try {
            $response =  ApiController::postApiRegister('register', $user);
            $code = $response->getStatusCode();
            $message =  $response->getData()->message[0];
            LogController::error(null, $response, 'Real server message', $code);
            if ($code == 200) {
                $message = "User Registered";
                return response()->json(["message" => [$message]], 200);
            }
            if (strpos($response, 'The password')) {
                $message = "The password you entered is incorrect. Please try again.";
                return response()->json(["message" => [$message]], 401);
            }
            if (strpos($response, 'Username or')) {
                $message = "Username or password aren't correct";
                return response()->json(["message" => [$message]], 402);
            }
            if (strpos($response, 'The username')) {
                $message = "The username you entered doesn't belong to an account. Please check your username and try again.";
                return response()->json(["message" => [$message]], 403);
            }
            if (strpos($response, 'Two Factor') || strpos($response, 'checkpoint')) {
                $message = "Two Factor Required";
                return response()->json(["message" => [$message]], 404);
            }
            if (strpos($response, 'User Registered')) {
                $message = "User Registered";
                return response()->json(["message" => [$message]], 405);
            }
            if (strpos($response, 'The code')) {
                $message = "The code is not correct";
                return response()->json(["message" => [$message]], 406);
            } else {
                $message = "Something went wrong";
                return response()->json(["message" => [$message]], 407);
            }
        } catch (\Exception $ex) {
            LogController::error(null, $ex->getMessage(), 'User Controller Register', $ex->getCode());
            throw new Exception($ex->getMessage(), 500);
        }
    }

    public function sendCode(Request $request)
    { }
    /**
     * @param Request $request
     * @return array|null|string
     * @throws \Exception
     */
    public function validator(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_configuration' => 'required'
        ]);
        if ($validator->fails()) {
            throw new \Exception($validator->errors());
        } else {
            return $request->input('user_configuration');
        }
    }
}
