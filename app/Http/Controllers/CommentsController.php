<?php

namespace App\Http\Controllers;

use App\Models\Comments;
use Illuminate\Http\Request;
use Validator;

class CommentsController extends Controller
{


    /**
     * @param Request $request
     * @throws \Exception
     */
    public function registerNewComment(Request $request)
    {
        try {
            $comment = $this->validator($request);
            $new_comment = new Comments();
            $user = auth()->userOrFail();
            $new_comment->comment = $comment;
            $new_comment->user_id = $user->user_id;
            $new_comment->save();
            return response()->json(["success" => "Comment created"], 200);
        } catch (\Exception $ex) {
            LogController::error(auth()->userOrFail(), $ex->getMEssage(), "REgister New Comment ", $ex->getCode());
            return response()->json(["error" => "Something went wrong"], 201);
        }
    }

    /**
     * @param Request $request
     * @return array|null|string
     * @throws \Exception
     */
    public function validator(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required|string',
        ]);
        if ($validator->fails()) {
            throw new \Exception($validator->errors());
        } else {
            return $request->input('comment');
        }
    }

    public function getAll()
    {
        try {
            $user = auth()->userOrFail();
            return Comments::where('user_id', $user->user_id)->get();
        } catch (\Exception $ex) {
            LogController::error(auth()->user(), $ex->getMessage(), 'Get all comments', $ex->getCode());
            return response()->json(["error" => "Something went wrong"], 201);
        }
    }
}
