<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReferencesController extends Controller
{
    public function createReferenceProfile(Request $request)
    {
        try {
            return ApiController::postApi('user/reference/profile/new', $request->all());
        } catch (\Exception $ex) {
            LogController::error(auth()->user(), $ex->getMessage(), 'Register reference profile', $ex->getCode());
            return response()->json(["error" => "Something went wrong"], 200);
        }
    }

    public function createReferenceHashtag(Request $request)
    {
        try {
            return ApiController::postApi('user/reference/hashtag/new', $request->all());
        } catch (\Exception $ex) {
            LogController::error(auth()->user(), $ex->getMessage(), 'Register hashtag', $ex->getCode());
            return response()->json(["error" => "Something went wrong"], 200);
        }
    }

    public function createReferenceLocation(Request $request)
    {
        try {
            return ApiController::postApi('user/reference/location/new', $request->all());
        } catch (\Exception $ex) {
            LogController::error(auth()->user(), $ex->getMessage(), 'Register location', $ex->getCode());
            return response()->json(["error" => "Something went wrong"], 200);
        }
    }

    public function findLocations($location)
    {
        try {
            $reference_location = ['reference_location' => $location];
            return ApiController::postApi('user/reference/location/find', $reference_location);
        } catch (\Exception $ex) {
            LogController::error(auth()->user(), $ex->getMessage(), 'Find Locations', $ex->getCode());
            return response()->json(["error" => "Something went wrong"], 200);
        }
    }
}
