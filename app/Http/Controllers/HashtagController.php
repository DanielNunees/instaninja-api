<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HashtagController extends Controller
{
    public static function taggedHashtag($tagged_hashtag)
    {
        try {
            $token = AuthController::apiLogin();
            $token_decoded =  json_decode($token);
            $endpoint = "http://localhost/instaninjaservice/public/api/tagged/hashtag";

            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', $endpoint, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token_decoded->token
                ],
                'form_params' => [
                    'hashtags' => $tagged_hashtag,
                ]
            ]);
            return $response->getBody()->getContents();
        } catch (\Exception $ex) {
            LogController::error(auth()->user(), $ex->getMessage(), 'Tagged Hashtags', $ex->getCode());
            return response()->json(["error" => "Something went wrong"], 200);
        }
    }
}
