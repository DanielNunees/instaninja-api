<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;

class AuthController extends Controller
{
    public static function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string',
            'password' => 'required|string'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $user = User::where('email', $request->email)->first();
        $payloadable = [
            'username' => $user->username,
            'profile_pic_url' => $user->profile_pic_url
        ];


        // all good so return the token
        $token = auth()->claims($payloadable)->attempt($credentials);

        //$token = response()->json(compact('token'));


        return response()->json(compact('token'));
    }


    /**
     * Api Loging using the IG credentials
     */

    public static function apiLogin()
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            $endpoint = "http://localhost/instaninjaservice/public/api/login";
            $client = new \GuzzleHttp\Client();
            $username = $user->username;
            $password = $user->ig_password;

            $response = $client->request('POST', $endpoint, [
                'form_params' => [
                    'username' => $username,
                    'ig_password' => $password,
                ]
            ]);
            $content = $response->getBody();


            $statusCode = $response->getStatusCode();
            $content = $response->getBody()->getContents();
            return $content;
        } catch (\Exception $ex) {
            return $ex;
        }
    }

    /**
     * Check if the user is already logedin
     */

    public function checkLogin()
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            $endpoint = "http://localhost/instaninjaservice/public/api/check";
            $client = new \GuzzleHttp\Client();
            $username = $user->username;
            $password = ($user->password_string);

            $response = $client->request('POST', $endpoint, [
                'form_params' => [
                    'username' => $username,
                    'password' => $password,
                ]
            ]);
            $content = $response->getBody();


            $statusCode = $response->getStatusCode();
            $content = $response->getBody()->getContents();
            return $content;
        } catch (\Exception $ex) {
            return $ex;
        }
    }

    public function register(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'username' => 'required|string',
                'password' => 'required',
                'email' => 'required',
                'confirmPassword' => 'required',
                'terms' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(["message" => [$validator->errors()]], 500);
            }
            $user = User::where('username', $request->input('username'))->orWhere('email', $request->input('email'))->first();

            if (is_null($user)) {
                $response = UserController::register($request->all());
            } else {
                $response = response()->json(
                    [
                        "message" => ["User is already registered"],
                    ],
                    403
                );
            }
            return response()->json($response->getData(), $response->getStatusCode());
        } catch (\Exception $ex) {
            LogController::error(null, $ex->getMessage(), 'Auth Controller Register', $ex->getCode());
            return response()->json(["message" => [($ex->getMessage())]], 403);
        }
    }

    public function logout()
    {
        $logout = auth()->logout();
        return response()->json($logout, 200);
    }

    public function me()
    {
        if (JWTAuth::parseToken()->authenticate()) {
            return JWTAuth::parseToken()->authenticate();
        } else {
            return false;
        }
    }
}
