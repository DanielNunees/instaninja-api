<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

class PeopleController extends Controller
{

    /**
     * @param Request $request
     * @return array|null|string
     * @throws \Exception
     */
    public function validator(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pattern' => 'required|string',
            'people' => 'required|array'
        ]);
        if ($validator->fails()) {
            throw new \Exception($validator->errors());
        } else {
            return ($request->all());
        }
    }
}
