<?php

namespace App\Http\Controllers;

use App\Models\ExceptionsLog;
use App\Models\UserLog;
use App\Models\SystemLog;

class LogController extends Controller
{

    public static function info($account, $code = null, $message = null)
    {
        $user_id = null;
        $username = null;
        if (!is_null($account)) {
            $username = $account->username;
            $user_id = $account->account_id;
        }
        UserLog::newLog($user_id, $username, $code, $message);
    }

    public static function error($account, $exception = null, $exception_type = null, $code = null)
    {
        $user_id = null;
        $username = null;
        if (!is_null($account)) {
            $username = $account->username;
            $user_id = $account->user_id;
        }

        ExceptionsLog::newLog($user_id, $username, $exception, $exception_type, $code);
    }

    public static function system($account, $exception = null, $exception_type = null, $code = null)
    {
        $user_id = null;
        $username = null;
        if (!is_null($account)) {
            $username = $account->username;
            $user_id = $account->account_id;
        }

        SystemLog::newLog($user_id, $username, $exception, $exception_type, $code);
    }
}
