<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'AuthController@login');
Route::post('register', 'AuthController@register');
Route::post('logout', 'AuthController@logout');
Route::post('refresh', 'AuthController@refresh');
Route::post('sendCode', 'AuthController@sendCode');
//Route::get('me', 'AuthController@me');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->fingerprint();
});


Route::group(['middleware' => ['jwt.verify']], function () {
    Route::get('/checkStatus', 'UserStatsController@checkStatus');

    // //Follow and Unfollow Routes

    Route::get('/action/follow/start', 'FollowersController@startFollowFollowers');
    Route::get('/action/follow/stop', 'FollowersController@stopFollowFollowers');

    Route::get('/action/likes/start', 'LikesController@startLikes'); //like Hashtag, my feed and location  medias
    Route::get('/action/likes/stop', 'LikesController@stopLikes'); //like Hashtag, my feed and location  medias

    Route::post('/posts/timeline/new', 'TimelineController@createNewPost');
    Route::get('/posts/timeline/{id}', 'TimelineController@getPost');
    Route::get('/posts/timeline', 'TimelineController@getAll');

    Route::post('/posts/story/new', 'StoryController@createNewStory');
    Route::get('/posts/story/{id}', 'StoryController@getPost');
    Route::get('/posts/story', 'StoryController@getAll');

    Route::post('/posts/comment/new', 'CommentsController@registerNewComment');
    Route::get('/posts/comments', 'CommentsController@getAll');

    Route::get('/posts/options', 'PostController@postOptions');



    Route::get('/logs/user/{quantity?}', 'UserStatsController@getUserLogs');
    Route::get('/logs/system/{quantity?}', 'UserStatsController@getSystemLogs');
    Route::get('/logs/exception/{quantity?}', 'UserStatsController@getExceptionLogs');
    Route::get('/user/stats/{quantity?}', 'UserStatsController@getUserStats');
    Route::get('/user/settings', 'UserController@getUserSettings');
    Route::get('/user/referenceProfiles', 'ReferenceProfileController@getAll');
    Route::get('/user/allReferences', 'UserController@getAllReferences');
    Route::post('/user/settings', 'UserController@registerUserSettings');
    Route::get("/test", "TestController@test");

    //References
    Route::post('/user/reference/profile/new', 'ReferencesController@createReferenceProfile');
    Route::post('/user/reference/hahstag/new', 'ReferencesController@createReferenceHashtag');
    Route::post('/user/reference/location/new', 'ReferencesController@createReferenceLocation');

    //Location
    Route::get('/user/reference/location/find/{location}', 'ReferencesController@findLocations');
});



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
